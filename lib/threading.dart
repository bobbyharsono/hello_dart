import 'dart:io';
import 'dart:async';
import 'dart:isolate';

Future checkVersion() async {
  var completer = Completer();
  var version = await lookupVersion();
  sleep(const Duration(seconds: 5));

  // populate with result
  completer.complete(version);

  // or use to get error
  // completer.complete(error)

  return completer.future;
}

String lookupVersion() => "2.2.0";

// using isolate
Isolate isolate;

void start() async {
  ReceivePort receivePort = ReceivePort();
  isolate = await Isolate.spawn(runTimer, receivePort.sendPort);
  receivePort.listen((data) {
    stdout.write('RECEIVE: ' + data + ', ');
  });
}

void stop() {
  if (null != isolate) {
    isolate.kill(priority: Isolate.immediate);
    isolate = null;
  }
}

void runTimer(SendPort sendPort) {
  int counter = 0;
  Timer.periodic(new Duration(seconds: 1), (Timer t) {
    counter++;
    String msg = 'notification ' + counter.toString();
    stdout.write('SEND: ' + msg + ' - ');
    sendPort.send(msg);
  });
}
