// generic map
var gifts = {
  // Key:    Value
  'first': 'partridge',
  'second': 'turtledoves',
  'fifth': 'golden rings'
};

// => is equal to -> in java
void showGift() => gifts.forEach((k, v) => print('${k}: ${v}'));

// [] is for optional position param
// {} is for optional named param
void say(String name, String message, [String device = 'telephone']) {
  var retval = "$name has message $message from $device";
  // ternary operator
  retval.length > 35 ? retval = "$retval long" : retval = "$retval short";
  // print to console
  print("$retval");
}

// trying to throw catch
void tryException() {
  var name = "Joko";
  try {
    if (name != "Joko") throw StupidException("Wrong Name Stupid Exception");
  } on StupidException catch (se) {
    print('Stupid Exception - $se');
  } finally {
    print("This is finally block");
  }
}

// custom exception
class StupidException implements Exception {
  StupidException(String cause);
}
