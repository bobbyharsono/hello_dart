// simple object
class Person {
  String name, address;
  int phone;
  Person(this.name, this.address, {this.phone});
}

// singleton with factory keyword in constructor
class PersonFactory {
  final int id;

  static final Map<int, PersonFactory> _cache = <int, PersonFactory>{};

  factory PersonFactory(int id) {
    if (_cache.containsKey(id)) {
      return _cache[id];
    } else {
      final personFactory = PersonFactory._internalConstructor(id);
      _cache[id] = personFactory;
      return personFactory;
    }
  }

  PersonFactory._internalConstructor(this.id);
}
