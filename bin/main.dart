import 'package:hello_dart/arithmetics.dart' as math;
import 'package:hello_dart/threading.dart' as thread;
import 'dart:io';

main(List<String> arguments) async{
  print('Hello world: ${math.calculate()}!');
  await thread.checkVersion().then(print);

  // isolate
  stdout.write('spawning isolate');
  await thread.start();
  stdout.write('press enter key to quit');
  await stdin.first;
  thread.stop();
  exit(0);
}


